﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Displays" Type="Folder">
		<Item Name="Template" Type="Folder">
			<Item Name="Template Indicators" Type="Folder">
				<Item Name="Numeric Indicator.ctl" Type="VI" URL="../Displays/Template/Numeric Indicator.ctl"/>
				<Item Name="Banner.ctl" Type="VI" URL="../Displays/Template/Indicators/Banner.ctl"/>
				<Item Name="Waveform Chart.ctl" Type="VI" URL="../Displays/Template/Waveform Chart.ctl"/>
			</Item>
			<Item Name="Data Display Template.vi" Type="VI" URL="../Displays/Template/Data Display Template.vi"/>
		</Item>
		<Item Name="Configurable DMC Style" Type="Folder">
			<Item Name="Data Display.vi" Type="VI" URL="../Displays/DMC Style/Data Display.vi"/>
		</Item>
		<Item Name="Low Resource" Type="Folder">
			<Item Name="Methods" Type="Folder">
				<Item Name="Dialogs" Type="Folder">
					<Item Name="Channel Select.vi" Type="VI" URL="../Displays/Low Resource/Methods/Dialogs/Channel Select.vi"/>
				</Item>
			</Item>
			<Item Name="Data Display Low Resource.vi" Type="VI" URL="../Displays/Low Resource/Data Display Low Resource.vi"/>
		</Item>
	</Item>
	<Item Name="Methods" Type="Folder">
		<Item Name="Dialogs" Type="Folder">
			<Item Name="Save.vi" Type="VI" URL="../Methods/Dialogs/Save.vi"/>
			<Item Name="Load.vi" Type="VI" URL="../Methods/Dialogs/Load.vi"/>
			<Item Name="Settings.vi" Type="VI" URL="../Methods/Dialogs/Settings.vi"/>
		</Item>
		<Item Name="Add Indicator.vi" Type="VI" URL="../Methods/Add Indicator.vi"/>
		<Item Name="Get Channel Names.vi" Type="VI" URL="../Methods/Get Channel Names.vi"/>
		<Item Name="Create Display Notifier.vi" Type="VI" URL="../Methods/Create Display Notifier.vi"/>
		<Item Name="Destroy Display Notifier.vi" Type="VI" URL="../Methods/Destroy Display Notifier.vi"/>
		<Item Name="Launch Data Display Asynchronously.vi" Type="VI" URL="../Methods/Launch Data Display Asynchronously.vi"/>
		<Item Name="Destroy Indicator Refs.vi" Type="VI" URL="../Methods/Destroy Indicator Refs.vi"/>
	</Item>
	<Item Name="Type Def" Type="Folder">
		<Item Name="Data to Display.ctl" Type="VI" URL="../Type Defs/Data to Display.ctl"/>
		<Item Name="Channel Names.ctl" Type="VI" URL="../Type Defs/Channel Names.ctl"/>
		<Item Name="Indicator References.ctl" Type="VI" URL="../Type Defs/Indicator References.ctl"/>
		<Item Name="Data Notifier.ctl" Type="VI" URL="../Type Defs/Data Notifier.ctl"/>
		<Item Name="Save Format.ctl" Type="VI" URL="../Type Defs/Save Format.ctl"/>
	</Item>
	<Item Name="Classes" Type="Folder">
		<Item Name="Indicators.lvclass" Type="LVClass" URL="../Classes/Indicators/Indicators.lvclass"/>
		<Item Name="Numeric.lvclass" Type="LVClass" URL="../Classes/Numeric/Numeric.lvclass"/>
		<Item Name="ChartWaveform.lvclass" Type="LVClass" URL="../Classes/ChartWaveform/ChartWaveform.lvclass"/>
	</Item>
	<Item Name="Tests" Type="Folder">
		<Item Name="Test Waveform Chart Properties.vi" Type="VI" URL="../Tests/Test Waveform Chart Properties.vi"/>
	</Item>
</Library>
